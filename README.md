**Objectif** : 
Réaliser un inventaire des projets d’éditions numériques de documents lexicographiques et de textes antiques (achevés ou en cours).

**Pour créer l'atelier Scenari Idkey 2** : 
Il est possible d'importer directement les fichiers contenus dans "sources".

**Fichier "Inventaire_projets.idkey"** : 
Clé de détermination qui permet d'assembler les critères, les entrées de taxonomie, les nœuds de taxons, les illustrations et les mentions légales dans la page d'accueil. 

**Dossier "critères"** (fichiers .criterion) : 
- Contient tous les critères et les valeurs rattachées aux taxons. Ils s'affichent sur la page d'accueil, les cocher permet de trouver un projet d'édition numérique (taxons).
- Chaque fichier est nommé d'après le premier mot de son identifiant (ex. "Opérations", pour "opérations réalisées et outils numériques associés").

**Dossier "ressources"** : 
Un dossier pour ranger les ressources (images, vidéos, audio, etc). 

**Dossier "taxons"** (fichiers .taxon) : 
Contient les notices de chaque projet.
- **Identifiants** : Identifiant du projet (Se trouve en haut de la notice, exemple : "lien vers l'ouvrage numérisé", "institution", "porteur du projet", etc)
- **Présentation** : Une courte présentation de 4 lignes max. du projet (elle apparaît dans l'onglet "classification" au niveau du projet).
- **Identification** : Les valeurs des critères rattachées au projet.
- **Description** : Bloc de texte servant à décrire le contenu et la valorisation du projet.
Il est aussi possible d'ajouter une licence et des illustrations.
Chaque fichier est nommé d'après le titre abrégé du projet.

**Dossier "taxonomie"** (fichiers .node) : 
Contient les noeuds de taxonomie servant à réaliser une classification des projets.
- Exemple : "Projets numériques" > "Ressources lexicographiques" > "Lexicographie de langue générale" > "Dictionnaire" > "Nénufar"
- Les nœuds sont rassemblés dans un sous-dossier (ANT pour textes antiques et LEX pour documents lexicographiques). Le dossier "LEX" est divisé en deux sous-dossiers intitulés : "1_lexicographie_générale" et "2_lexicographie_specialité", chacun contenant quatre noeuds : "dictionnaire", "encyclopédie", "glossaires, lexiques, index" et "autre".
- Les nœuds sont consultables en cliquant sur les onglets "parcourir la taxonomie" et "classification" (dans la notice du projet).

**Dossier "critères_non_utilisés"**  : 
Contient les critères non retenus.
